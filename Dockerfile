# Start your image with a python base image
FROM python:3.9-slim

# The /app directory should act as the main application directory
WORKDIR /app

# Copy the requirements.txt file to the container
COPY requirements.txt .

# Install the required Python packages
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code to the container
COPY . .

# Expose the port Streamlit will run on
EXPOSE 8501

# Start the app using streamlit run command
CMD ["streamlit", "run", "index.py"]